#!/usr/bin/python3

import random
import os
import _Getch
import time
import sys

sys.setrecursionlimit(10000)

cursorX = 0
cursorY = 0
dead=False
minesLeft=0
totalMines=10
width, height=10,20
map = []
cursorChar = "☺"
moves=0

start_time = 0


class Tile(object):
    mine=False
    revealed=False
    flagged=False
    neighbour=0
    x=0
    y=0
    upperLimitX, upperLimitY = 0, 0
    mineIcon = "☼"
    flagIcon = "F"
    
    def __init__(self,x, y, upperLimitX, upperLimitY):
        self.x=x
        self.y=y
        self.upperLimitX = upperLimitX
        self.upperLimitY = upperLimitX

    def setFlag(self):
        self.flagged=True
    def removeFlag(self):
        self.flagged=False

    def isFlagged(self):
        return self.flagged

    def isRevealed(self):
        return self.revealed

    def countN(self,map, upperLimitX, upperLimitY):
        total=0
        if self.isMined()==False:
            for yoff in range(-1,2):
                for xoff in range(-1,2):
                    i=self.x+xoff
                    j=self.y+yoff
                    if i > -1 and i<upperLimitX and j > -1 and j < upperLimitY:
                        if map[i][j].isMined()==True:
                            total+=1
            self.setNeighbour(total)
                            
                                                              
    def clamp(self, n, minn, maxn):
        if n < minn:
            return minn
        elif n > maxn:
            return maxn
        else:
            return n
            
    def isMined(self):
        return self.mine
    
    def addMine(self):
        self.mine=True
        self.neighbour=-1

    def reveal(self):
        if self.revealed == False:
            self.revealed=True
            if self.isMined()==False and self.neighbour == 0:
                for yoff in range(-1,2):
                    for xoff in range(-1,2):
                        i=self.x+xoff
                        j=self.y+yoff
                        if i > -1 and i< self.upperLimitX and j > -1 and j < self.upperLimitY:
                            map[i][j].reveal()

        
    def setNeighbour(self,number):
        self.neighbour=number
        
    def show(self):
        if self.revealed:
            if self.neighbour==-1:
                return self.mineIcon
            else:
                if self.neighbour==0:
                    return " "
                else:
                    return self.neighbour
        else:
            if self.isFlagged():
                return self.flagIcon
            else:
                return "■"

#Main loop
def main():
    global moves
    global totalMines
    global minesLeft
    global width
    global height
    global start_time
    print ()
    print (" ╔════════════════════════════╗")
    print (" ║ pMinesweeper ☼ ☼ ☼ ☼ ☼ ☼ ☼ ║")
    print (" ║ by Alexander Jonsson, 2017 ║")
    print (" ╚════════════════════════════╝")
    print (" ╔════════════════════════════╗")
    print (" ║  ☼   TOP FIVE RANKING   ☼  ║")
    print (" ╠════════════════════════════╣")
    print (" ║ 1. Donald J. Trump      1s ║")
    print (" ║ 2. Steve Jobs         234s ║")
    print (" ║ 3. Carl XVI Gustav    343s ║")
    print (" ║ 4. Gustav Vasa        821s ║")
    print (" ║ 5. Alex Jones        1433s ║")   
    print (" ╚════════════════════════════╝")
    print ()
    print (" Gameplay options")
    try:
        width = int(input(" Board size (10): "))
    except:
        width = 10
    width = clamp (width, 2,90)
    height = width
    try:
        totalMines = int(input (" Mines(10): "))
        totalMines = clamp (totalMines, 1,width*height-1)
    except:
        totalMines = 10
    minesLeft = totalMines
    
    global cursorX, cursorY
    initMap()
    cmd = ""
    start_time = time.time()

    if os.name=="nt":   
        while cmd!=b'\x1b':
            print (cursorX, cursorY)
            draw(map)
            cmd = _Getch.getch.impl()
            if cmd == b' ':
                map[cursorX][cursorY].reveal()
                moves+=1
                winlose()
            elif cmd == b'f':
                if map[cursorX][cursorY].isFlagged() == True:
                    map[cursorX][cursorY].removeFlag()
                else:
                    map[cursorX][cursorY].setFlag()
            elif cmd == b'w':
                cursorY -= 1
            elif cmd == b'a':
                cursorX -= 1
            elif cmd == b's':
                cursorY += 1
            elif cmd == b'd':
                cursorX += 1
            cursorX = clamp(cursorX, 0, height-1)
            cursorY = clamp(cursorY, 0, width-1)

    if os.name=="posix":   
        while cmd!='q':
            print (cursorX, cursorY)
            draw(map)
            cmd = _Getch.getch.impl()
            if cmd == ' ':
                map[cursorX][cursorY].reveal()
                moves+=1
                winlose()
            elif cmd == 'f':
                if map[cursorX][cursorY].isFlagged() == True:
                    map[cursorX][cursorY].removeFlag()
                else:
                    map[cursorX][cursorY].setFlag()
            elif cmd == 'w':
                cursorY -= 1
            elif cmd == 'a':
                cursorX -= 1
            elif cmd == 's':
                cursorY += 1
            elif cmd == 'd':
                cursorX += 1
            cursorX = clamp(cursorX, 0, height-1)
            cursorY = clamp(cursorY, 0, width-1)


def countFlags(map):
    temp = minesLeft
    for i in range(height):
        for j in range(width):
            if map[i][j].isFlagged() == True:
                temp -= 1
    return temp
            
def draw(map):
    clearScreen()
    print ()
    print  (" MINES:",clamp(countFlags(map),0,totalMines), " MOVES:", moves)
    print ("┌", end="")
    for a in range(len(map)):
        print ("─", end="─")
    print("┐")
    for a in range(len(map)):
        print ("│", end="")
        for b in range(len(map[a])):
            if b == cursorX and a == cursorY and dead == False:
                print (cursorChar, end=" ")
            else:
                print (map[b][a].show(), end=" ")
        print("│")
    print ("└",end="")
    for a in range(len(map)):
        print ("─", end="─")
    print("┘")
    if os.name=="nt":
        print ("w, a, s, d=Move, space=Dig, f=Toggle flag, ESC=Quit")
    if os.name=="posix":
        print ("w, a, s, d=Move, sapce=Dig, f=Toggle flag, q=Quit")

#randomly add a number of mines to the map
def addMines(limit):
    print ("Adding mines...")
    l2=limit
    for limit in range(l2):
        r1, r2 =random.randint(0,width-1), random.randint(0,height-1)
        if map[r1][r2].isMined():
            addMines(l2)
            break
        else:
            map[r1][r2].addMine()
            l2=l2-1

#Create the map itself
def initMap():
    print ("Creating map...")
    for i in range(height):
        map.append([])
        for j in range(height):
            map[i].append(Tile(i,j,width, height))
    addMines(totalMines)
    print ("Counting mines")
    for i in range(width):
        for j in range(height):
            map[i][j].countN(map,width, height)                                            

#limit the value of a variable
def clamp(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n

def clearScreen():
    if os.name=="nt":
        os.system("cls")
    else:
        os.system("clear")

def winlose():
    tilesLeft=0
    global dead
    #global minesLeft
    if map[cursorX][cursorY].isMined()==True:
        dead = True
        for i in range (height):
            for j in range (width):
                map[i][j].revealed = True
        draw(map)
        print ("YOU LOSE!!!!!")
        final_time = time.time()-start_time
        print ("Time taken: ", int(final_time),"s")
        quit()
    for i in range (height):
        for j in range (width):
           if map[i][j].isMined() == False and map[i][j].isRevealed() == False:
               tilesLeft += 1
    if tilesLeft == 0:
        for i in range (height):
            for j in range (width):
                map[i][j].revealed = True
                if map[i][j].isMined() == True:
                    map[i][j].mineIcon = map[i][j].flagIcon
                    dead = True
        draw(map)
        print ("YOU WIN!")
        final_time = time.time()-start_time
        print ("Time taken: ", int(final_time),"s")
        quit()
               
        

clearScreen()
main()
